﻿#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "stm32f10x_conf.h"
#include "stm32f103.h"
#include "serial.h"
#include "timeup.h"
#include "ssd1306.h"
#include "data.h"
#include "idog.h"
#include "display.h"
#include "settings.h"
#include <fonts/font.h>

uint8_t buff[DMA_BUFFER_SIZE];
uint8_t silentMode = 0;


extern local_data_t local_data;
extern trip_data_t trip_data;

const uint8_t logo[] = {
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x00, 0x1F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x07, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x0F, 0xFF, 0xE0, 0x00, 0x01, 0x80, 0x00, 0x0F, 0xFF, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x1F, 0xFF, 0xE0, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};


int main ( void )
{

	local_data.S20C00HZ65.throttle = 0;
	local_data.S20C00HZ65.brake = 0;
	local_data.S25C31.remainCapacity = 0;
	local_data.S25C31.remainPercent = 0;
	local_data.S25C31.current = 0;
	local_data.S23CB0.speed = 0;
#if 0
	local_data.S20C00HZ65.throttle = 40;
	local_data.S20C00HZ65.brake = 40;
	local_data.S25C31.remainCapacity = 12634;
	local_data.S25C31.remainPercent = 98;
	local_data.S25C31.current = 0;
	local_data.S23CB0.speed = 0;
#endif

	/* Setup STM32 system (clocks, Ethernet, GPIO, NVIC) */
	System_Setup();

	load_settings();

	PrepareReceiveCOM ( DEFCOM, &buff[0], DMA_BUFFER_SIZE );

	ssd1306_Init();
	ssd1306_SetCursor ( 0, 0 );
	ssd1306_DrawBitmap ( logo, 128, 64, White );
	ssd1306_UpdateScreen();

	Delay ( 2000 );	//Задержка, чтобы успеть зажать газ и тормоз для прошивки

	while ( ( local_data.S20C00HZ65.throttle == 0 ) && ( local_data.S20C00HZ65.brake == 0 ) ) { //Ждем данных от сяо
		Delay ( 100 );
	}

	if ( ( local_data.S20C00HZ65.throttle >= 100 ) &&
			( local_data.S20C00HZ65.brake >=
			  100 ) ) { //Если нажаты тормоз и газ включаем режим радиомолчания для прошивки. В отпущенном состоянии значения около 40;
		silentMode = 1;
	} else {
		while ( ( local_data.S25C31.remainCapacity == 0 ) && ( local_data.S25C31.remainPercent == 0 ) ) { //Ждем запрошенные данные от сяо
			Delay ( 100 );
		}
	}

	trip_data.startCapacity = local_data.S25C31.remainCapacity;
	trip_data.startPercent = local_data.S25C31.remainPercent;

	//	IWDG_Init(4, 2500);
	TIM_Cmd ( TIM4, ENABLE ); //Обработка кнопок и max величин

	/* Infinite loop */
	while ( 1 ) {
		//		IWDG_Feed();
		UpdateScreen();
		Delay ( 200 );
	}
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed ( uint8_t *file, uint32_t line )
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while ( 1 )
	{}
}
#endif
