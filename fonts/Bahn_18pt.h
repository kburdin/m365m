#ifndef __BAHN_18PT_H__
#define __BAHN_18PT_H__

/* Font data for Bahnschrift SemiLight Condensed 18pt */
extern const uint8_t bahnschriftSemiLightCondensed_18ptBitmaps[];
extern const FONT_INFO bahn18pt;
extern const FONT_CHAR_INFO bahnschriftSemiLightCondensed_18ptDescriptors[];

#endif
