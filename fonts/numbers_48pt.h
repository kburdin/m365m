#ifndef __NUMBERS_48PT_H__
#define __NUMBERS_48PT_H__

extern const uint8_t bahnschriftSbahnschriftSemiBoldSemiConden_48ptBitmaps[];
extern const FONT_INFO numbers_48pt_font;
extern const FONT_CHAR_INFO bahnschriftSemiBoldSemiConden_48ptDescriptors[];

#endif
