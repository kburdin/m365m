#ifndef __FONT_H__
#define __FONT_H__

/* Fonts support for The Dot Factory v.0.1.4 generated fonts (http://www.eran.io/the-dot-factory-an-lcd-font-and-image-generator/) */
typedef unsigned char uint8_t;


typedef struct {
	int width; // Character width in bits.
	int offset; // Offset in bytes into font bitmap.
}
FONT_CHAR_INFO;


typedef struct {
	int height; // Character height in bits.
	char start_char; // Start character.
	char end_char;
	const FONT_CHAR_INFO *p_character_descriptor; // Character decriptor array.
	const uint8_t *p_character_bitmaps; // Character bitmap array.
} FONT_INFO;


#endif // __FONT_H__
