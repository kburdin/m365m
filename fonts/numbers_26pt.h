#ifndef __NUMBERS_26PT_H__
#define __NUMBERS_26PT_H__

/* Font data for Bahnschrift SemiLight Condensed 26pt */
extern const uint8_t bahnschriftSemiLightCondensed_26ptBitmaps[];
extern const FONT_INFO numbers_26pt_font;
extern const FONT_CHAR_INFO bahnschriftSemiLightCondensed_26ptDescriptors[];

#endif
