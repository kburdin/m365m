#ifndef __BAHN_12PT_H__
#define __BAHN_12PT_H__

/* Font data for Bahnschrift SemiLight Condensed 10pt */
extern const uint8_t bahnschriftSemiLightCondensed_10ptBitmaps[];
extern const FONT_INFO bahn12pt;
extern const FONT_CHAR_INFO bahnschriftSemiLightCondensed_10ptDescriptors[];

#endif
