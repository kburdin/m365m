#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "stm32f10x_conf.h"
#include "ssd1306.h"
#include "data.h"
#include "display.h"
#include "timeup.h"
#include "settings.h"
#include <fonts/Bahn_18pt.h>
#include <fonts/Bahn_12pt.h>
#include <fonts/numbers_26pt.h>
#include <fonts/numbers_48pt.h>

void MainScreen ( void );
void TripScreen ( void );
void MenuScreen ( void );
void SettingsScreen ( void );
void SetupScreen ( void );
void BattScreen ( void );
void SInfoScreen ( void );
void RideScreen ( void );
void ChargeScreen ( void );

settings_t settings;
screens_t screens = { 0, 0, { MainScreen, TripScreen, MenuScreen, BattScreen,
							  SInfoScreen, SetupScreen, SettingsScreen, RideScreen, ChargeScreen
							}
					};

char menu_entries[][16] = { " Battery Info ", " Scooter Info ", " Scooter Setup ", " Settings " };
const uint8_t menu_enries_cnt = sizeof ( menu_entries ) / 16;

const char Ride_opts[3][8] = { "Off", "Speed", "Curr." };
const char OnOff_opts[2][8] = { "Off", "On" };
const char Kers_opts[3][8] = { "Low", "Med", "High" };

const uint8_t ypos[] = { 18, 34, 50 };

uint8_t button;

extern requests_t requestsSI[];
extern requests_t requestsST[];
extern commands_t commands[];
extern local_data_t local_data;
extern trip_data_t trip_data;
extern uint8_t silentMode;

void UpdateScreen()
{
	ssd1306_Fill ( Black );
#if 0

	switch ( screens.screen ) {
		case 0:
		case 1:
			switch ( button ) {
				case Brake_short:
					break;

				case Brake_long:
					screens.screen = 1;				//переходим на экран Trip
					screens.subscreen = 0;
					break;

				case Thr_short:
					screens.subscreen++;			//Следующий набор данных
					break;

				case Thr_long:
					screens.screen = 0;				//переходим на экран Main
					screens.subscreen = 0;
					break;

				case Both_long:
					screens.screen = 2;				//переходим на экран Menu
					screens.subscreen = 0;
					break;

				default:
					break;
			}

			button = NONE;	//отработано
			break;

		default:
			break; //В остальных случах кнопка отрабатываеся в функции самого экрана
	}

#endif
	screens.FillInfo[screens.screen]();

	ssd1306_UpdateScreen();
}

void RideScreen()
{
	char buf[8];

	ssd1306_Fill ( Black );
	ssd1306_SetCursor ( 25, 8 );
	snprintf ( buf, sizeof ( buf ), "%02.1f",
			   ( settings.ridescr == 1 ) ?
			   ( local_data.S23CB0.speed / 1000.0 * settings.wheelmult ) :
			   ( local_data.S25C31.current / 100.0 ) );
	ssd1306_WriteString ( buf, &numbers_48pt_font, White );
	ssd1306_UpdateScreen();
	Delay ( 1000 );
}

void ChargeScreen()
{
	char buf[32];

	ssd1306_SetCursor ( 25, 0 );
	ssd1306_WriteString ( "CHARGING", &bahn12pt, White );
	ssd1306_drawHLine ( 0, 13, 128, White );

	switch ( screens.subscreen ) {
		case 0:
			snprintf ( buf, sizeof ( buf ), "Charge:       %d %%",
					   local_data.S25C31.remainPercent );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Capacity:     %-5d mAh",
					   local_data.S25C31.remainCapacity );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Current:       %2.1fA",
					   ( local_data.S25C31.current / 100.0 ) * - 1 );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		case 1:
			snprintf ( buf, sizeof ( buf ), " 1=%1.2f   2=%1.2f   3=%1.2f",
					   local_data.S25C40.cell[0] / 1000.0,
					   local_data.S25C40.cell[1] / 1000.0,
					   local_data.S25C40.cell[2] / 1000.0 );
			ssd1306_SetCursor ( 0, 15 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "4=%1.2f   5=%1.2f   6=%1.2f",
					   local_data.S25C40.cell[3] / 1000.0,
					   local_data.S25C40.cell[4] / 1000.0,
					   local_data.S25C40.cell[5] / 1000.0 );
			ssd1306_SetCursor ( 0, 27 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "7=%1.2f   8=%1.2f   9=%1.2f",
					   local_data.S25C40.cell[6] / 1000.0,
					   local_data.S25C40.cell[7] / 1000.0,
					   local_data.S25C40.cell[8] / 1000.0 );
			ssd1306_SetCursor ( 0, 39 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Total=%2.1f V      10=%1.2f",
					   local_data.S25C31.voltage / 100.0,
					   local_data.S25C40.cell[9] / 1000.0 );
			ssd1306_SetCursor ( 0, 51 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		default:
			screens.subscreen = 0;
	}

	switch ( button ) {
		case Brake_long:
			break;

		case Thr_short:
			screens.subscreen++;
			break;

		case Thr_long:
			break;

		case Both_long:
			break;

		default:
			break;
	}

	button = NONE;
}

void SavedScreen()
{
	ssd1306_Fill ( Black );
	ssd1306_SetCursor ( 35, 26 );
	ssd1306_WriteString ( "Saved", &bahn12pt, White );
	ssd1306_UpdateScreen();
	Delay ( 1000 );
}

void MainScreen()
{
	char buf[64];

	snprintf ( buf, sizeof ( buf ), "%02.1f V", local_data.S25C31.voltage / 100.0 );
	ssd1306_SetCursor ( 0, 0 );
	ssd1306_WriteString ( buf, &bahn18pt, White );

	snprintf ( buf, sizeof ( buf ), "%3d %%", local_data.S25C31.remainPercent );
	ssd1306_SetCursor ( 85, 0 );
	ssd1306_WriteString ( buf, &bahn18pt, White );

	snprintf ( buf, sizeof ( buf ), "%02d:%02d", local_data.S23C3A.powerOnTime / 60,
			   local_data.S23C3A.powerOnTime % 60 );
	ssd1306_SetCursor ( 0, 46 );
	ssd1306_WriteString ( buf, &bahn18pt, White );

	snprintf ( buf, sizeof ( buf ), "%2d DC", local_data.S23CB0.mainframeTemp / 10 );
	ssd1306_SetCursor ( 85, 46 );
	ssd1306_WriteString ( buf, &bahn18pt, White );

	if ( silentMode ) {
		ssd1306_SetCursor ( 35, 26 );
		ssd1306_WriteString ( "Silent Mode", &bahn12pt, White );
	} else {
		switch ( screens.subscreen ) {
			case 0:
				snprintf ( buf, sizeof ( buf ), "%2.2fB",
						   ( local_data.S23CB0.mileageCurrent / 100.0 )
						   * settings.wheelmult );
				ssd1306_SetCursor ( 31, 19 );
				ssd1306_WriteString ( buf, &numbers_26pt_font, White );
				break;

			case 1:
				snprintf ( buf, sizeof ( buf ), "%2.2fA",
						   ( local_data.S23CB0.mileageTotal / 1000.0 )
						   * settings.wheelmult );
				ssd1306_SetCursor ( 21, 19 );
				ssd1306_WriteString ( buf, &numbers_26pt_font, White );
				break;

			case 2:
				if ( ( settings.calcld == 0 )
						|| ( ( local_data.S23CB0.mileageCurrent * settings.wheelmult )
							 < 100 ) ) { //Отключен расчетный пробег или пробег за поездку менее 100м.
					snprintf ( buf, sizeof ( buf ), "%2.2fC",
							   ( local_data.S23C25.remainMileage / 100.0 )
							   * settings.wheelmult );
				} else {
					snprintf ( buf, sizeof ( buf ), "%2.2fC",
							   ( ( local_data.S25C31.remainCapacity
								   / ( ( trip_data.startCapacity
										 - local_data.S25C31.remainCapacity )
									   / local_data.S23CB0.mileageCurrent ) ) / 100 )
							   * settings.wheelmult );
				}

				ssd1306_SetCursor ( 31, 19 );
				ssd1306_WriteString ( buf, &numbers_26pt_font, White );
				break;

			default:
				screens.subscreen = 0;
				break;
		}

		switch ( button ) {
			case Brake_short:
				break;

			case Brake_long:
				screens.screen = 1;				//переходим на экран Trip
				screens.subscreen = 0;
				break;

			case Thr_short:
				screens.subscreen++;			//Следующий набор данных
				break;

			case Thr_long:
				GPIO_ToggleBits ( GPIOB, GPIO_Pin_13 );
				break;

			case Both_long:
				screens.screen = 2;				//переходим на экран Menu
				screens.subscreen = 0;
				break;

			default:
				break;
		}

		button = NONE;	//отработано
	}
}

void TripScreen()
{
	char buf[64];

	ssd1306_SetCursor ( 40, 0 );
	ssd1306_WriteString ( "TRIP INFO", &bahn12pt, White );
	ssd1306_drawHLine ( 0, 13, 128, White );

	switch ( screens.subscreen ) {
		case 0:
			snprintf ( buf, sizeof ( buf ), "Distance:     %2.2fKm",
					   ( local_data.S23CB0.mileageCurrent / 100.0 )
					   * settings.wheelmult );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Ride time:    %02d:%02d",
					   local_data.S23C3A.ridingTime / 60,
					   local_data.S23C3A.ridingTime % 60 );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Batt used:    %03d%%",
					   trip_data.startPercent - local_data.S25C31.remainPercent );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		case 1:
			snprintf ( buf, sizeof ( buf ), "Avg speed:    %02.1f km/h",
					   ( local_data.S23CB0.averageSpeed / 1000.0 ) * settings.wheelmult );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Max speed:    %02.1f km/h",
					   ( trip_data.maxSpeed / 1000.0 ) * settings.wheelmult );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Max current:    %2.1fA",
					   trip_data.maxCurrent / 100.0 );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		default:
			screens.subscreen = 0;
			break;
	}

	switch ( button ) {
		case Brake_short:
			break;

		case Brake_long:
			screens.screen = 0;				//переходим на экран Main
			screens.subscreen = 0;
			break;

		case Thr_short:
			screens.subscreen++;			//Следующий набор данных
			break;

		case Thr_long:
			break;

		case Both_long:
			break;

		default:
			break;
	}

	button = NONE;	//отработано

}

void BattScreen()
{
	char buf[64];

	ssd1306_SetCursor ( 20, 0 );
	ssd1306_WriteString ( "BATTERY INFO", &bahn12pt, White );
	ssd1306_drawHLine ( 0, 13, 128, White );

	switch ( screens.subscreen ) {
		case 0:
			snprintf ( buf, sizeof ( buf ), "Capacity:        %-5d mAh",
					   local_data.S25C10.capacity );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Remaining:     %-5d mAh",
					   local_data.S25C31.remainCapacity );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Temp:             %02d C  %02d C",
					   local_data.S25C31.temp1 - 20, local_data.S25C31.temp2 - 20 );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		case 1:
			snprintf ( buf, sizeof ( buf ), " 1=%1.2f   2=%1.2f   3=%1.2f",
					   local_data.S25C40.cell[0] / 1000.0,
					   local_data.S25C40.cell[1] / 1000.0,
					   local_data.S25C40.cell[2] / 1000.0 );
			ssd1306_SetCursor ( 0, 15 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "4=%1.2f   5=%1.2f   6=%1.2f",
					   local_data.S25C40.cell[3] / 1000.0,
					   local_data.S25C40.cell[4] / 1000.0,
					   local_data.S25C40.cell[5] / 1000.0 );
			ssd1306_SetCursor ( 0, 27 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "7=%1.2f   8=%1.2f   9=%1.2f",
					   local_data.S25C40.cell[6] / 1000.0,
					   local_data.S25C40.cell[7] / 1000.0,
					   local_data.S25C40.cell[8] / 1000.0 );
			ssd1306_SetCursor ( 0, 39 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Total=%2.1f V      10=%1.2f",
					   local_data.S25C31.voltage / 100.0,
					   local_data.S25C40.cell[9] / 1000.0 );
			ssd1306_SetCursor ( 0, 51 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		case 2:
			snprintf ( buf, sizeof ( buf ), "Date:     %02d.%02d.%02d",
					   local_data.S25C20.day, local_data.S25C20.month,
					   local_data.S25C20.year );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "S/N: %s", local_data.S25C10.serial );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Status:    %02d%%",
					   local_data.S25C3B.status );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		case 3:
			snprintf ( buf, sizeof ( buf ), "Charge count:    %d",
					   local_data.S25C1B.charges );
			ssd1306_SetCursor ( 0, 18 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "Full charges:     %d",
					   local_data.S25C1B.fullcharges );
			ssd1306_SetCursor ( 0, 34 );
			ssd1306_WriteString ( buf, &bahn12pt, White );

			snprintf ( buf, sizeof ( buf ), "BMS ver:           %x",
					   local_data.S25C10.bmsver );
			ssd1306_SetCursor ( 0, 50 );
			ssd1306_WriteString ( buf, &bahn12pt, White );
			break;

		default:
			screens.subscreen = 0;
			break;
	}

	switch ( button ) {
		case Brake_long:
			break;

		case Thr_short:
			screens.subscreen++;
			break;

		case Thr_long:
			break;

		case Both_long:
			screens.screen = 0;				//переходим на экран Main
			screens.subscreen = 0;
			break;

		default:
			break;
	}

	button = NONE;
}

void MenuScreen()
{
	int8_t start, stop, pos = 0;
	ssd1306_SetCursor ( 40, 0 );
	ssd1306_WriteString ( "MENU", &bahn12pt, White );
	ssd1306_drawHLine ( 0, 13, 128, White );

	start = screens.subscreen - 1;
	stop = screens.subscreen + 1;

	if ( start < 0 ) {
		start++;
		stop++;
	}

	if ( stop >= menu_enries_cnt )
	{ stop--; }

	for ( uint8_t i = start; i <= stop; i++ ) {
		ssd1306_SetCursor ( 20, ypos[pos] );
		ssd1306_WriteString ( menu_entries[i], &bahn12pt,
							  ( i == screens.subscreen ) ? Black : White );
		pos++;
	}

	switch ( button ) {									//Обработка кнопок
		case Brake_short:									//Следующий пункт
			break;

		case Brake_long:
			break;

		case Thr_short:
			screens.subscreen++;
			screens.subscreen %= menu_enries_cnt;
			break;

		case Thr_long:
			screens.screen = 3 + screens.subscreen;	//переходим на выбранный экран
			screens.subscreen = 0;
			break;

		case Both_long:
			screens.screen = 0;					//переходим на экран Main
			screens.subscreen = 0;
			break;

		default:
			break;
	}

	button = NONE;
}

void SettingsScreen()
{
	char buf[64];
	uint8_t cnt = 0;
	uint8_t cpos = 0;
	uint8_t vpos = 18;

	ssd1306_SetCursor ( 40, 0 );
	ssd1306_WriteString ( "SETTINGS", &bahn12pt, White );
	ssd1306_drawHLine ( 0, 13, 128, White );

	if ( screens.subscreen == 3 ) {
		cpos = 1;
	}

	while ( cnt < 3 ) {
		switch ( cpos ) {
			case 0:
				snprintf ( buf, sizeof ( buf ), "Ride screen:       %s  ", Ride_opts[settings.ridescr] );
				ssd1306_SetCursor ( 0, vpos );
				ssd1306_WriteString ( buf, &bahn12pt, ( screens.subscreen == 0 ) ? Black : White );
				break;

			case 1:
				snprintf ( buf, sizeof ( buf ), "Wheel mult.:        %1.2f  ", settings.wheelmult );
				ssd1306_SetCursor ( 0, vpos );
				ssd1306_WriteString ( buf, &bahn12pt, ( screens.subscreen == 1 ) ? Black : White );
				break;

			case 2:
				snprintf ( buf, sizeof ( buf ), "Calc. left dist:      %s  ", OnOff_opts[settings.calcld] );
				ssd1306_SetCursor ( 0, vpos );
				ssd1306_WriteString ( buf, &bahn12pt, ( screens.subscreen == 2 ) ? Black : White );
				break;

			case 3:
				snprintf ( buf, sizeof ( buf ), "Ext. light inv.:      %s  ", OnOff_opts[settings.light] );
				ssd1306_SetCursor ( 0, vpos );
				ssd1306_WriteString ( buf, &bahn12pt, ( screens.subscreen == 3 ) ? Black : White );
				break;

			default:
				break;
		}

		vpos += 16;
		cpos ++;
		cnt ++;
	}

	switch ( button ) {										//Обработка кнопок
		case Brake_short:									//Следующий пункт
			switch ( screens.subscreen ) {
				case 0:
					settings.ridescr += 1;
					settings.ridescr %= 3;
					break;

				case 1:
					settings.wheelmult += 0.01;

					if ( settings.wheelmult > 1.2 )
					{ settings.wheelmult = 1.0; }

					break;

				case 2:
					settings.calcld += 1;
					settings.calcld %= 2;
					break;

				case 3:
					settings.light += 1;
					settings.light %= 2;
					break;

				default:
					break;
			}

			break;

		case Brake_long:
			break;

		case Thr_short:
			screens.subscreen++;
			screens.subscreen %= 4;
			break;

		case Thr_long:
			save_settings();
			SavedScreen();
			break;

		case Both_long:
			screens.screen = 0;				//переходим на экран Main
			screens.subscreen = 0;
			break;

		default:
			break;
	}

	button = NONE;
}

void SetupScreen()
{
	char buf[64];
	static uint16_t params[3] = { 0xFFFF, 0xFFFF, 0xFFFF };

	if ( requestsST[0].isEnabled ) {
		ssd1306_SetCursor ( 25, 26 );
		ssd1306_WriteString ( "Waiting data", &bahn12pt, White );
	} else {
		if ( local_data.S23C7B.tailled == 2 )
		{ local_data.S23C7B.tailled = 1; }

		if ( params[0] == 0xFFFF )
		{ params[0] = local_data.S23C7B.tailled; }

		if ( params[1] == 0xFFFF )
		{ params[1] = local_data.S23C7B.cruise; }

		if ( params[2] == 0xFFFF )
		{ params[2] = local_data.S23C7B.kers; }

		ssd1306_SetCursor ( 45, 0 );
		ssd1306_WriteString ( "SETUP", &bahn12pt, White );
		ssd1306_drawHLine ( 0, 13, 128, White );

		snprintf ( buf, sizeof ( buf ), "Tail LED:        %s  ",
				   OnOff_opts[params[0]] );
		ssd1306_SetCursor ( 0, 18 );
		ssd1306_WriteString ( buf, &bahn12pt,
							  ( screens.subscreen == 0 ) ? Black : White );

		snprintf ( buf, sizeof ( buf ), "Cruise:            %s  ",
				   OnOff_opts[params[1]] );
		ssd1306_SetCursor ( 0, 34 );
		ssd1306_WriteString ( buf, &bahn12pt,
							  ( screens.subscreen == 1 ) ? Black : White );

		snprintf ( buf, sizeof ( buf ), "KERS:             %s  ",
				   Kers_opts[params[2]] );
		ssd1306_SetCursor ( 0, 50 );
		ssd1306_WriteString ( buf, &bahn12pt,
							  ( screens.subscreen == 2 ) ? Black : White );

		switch ( button ) {								//Обработка кнопок
			case Brake_short:								//Следующий пункт
				params[screens.subscreen] += 1;

				switch ( screens.subscreen ) {
					case 2:
						params[screens.subscreen] %= 3;
						break;

					default:
						params[screens.subscreen] %= 2;
				}

				break;

			case Brake_long:
				break;

			case Thr_short:
				screens.subscreen++;
				screens.subscreen %= 3;
				break;

			case Thr_long:
				if ( params[0] != local_data.S23C7B.tailled )
				{ commands[5 + params[0]].isEnabled = 1; }

				if ( params[1] != local_data.S23C7B.cruise )
				{ commands[3 + params[1]].isEnabled = 1; }

				if ( params[2] != local_data.S23C7B.kers )
				{ commands[0 + params[2]].isEnabled = 1; }

				requestsST[0].isEnabled = 1;
				SavedScreen();
				break;

			case Both_long:
				screens.screen = 0;				//переходим на экран Main
				screens.subscreen = 0;
				requestsST[0].isEnabled = 1;
				break;

			default:
				break;
		}

		button = NONE;
	}
}

void SInfoScreen()
{
	char buf[64];

	if ( requestsSI[0].isEnabled ) {
		ssd1306_SetCursor ( 25, 26 );
		ssd1306_WriteString ( "Waiting data", &bahn12pt, White );
	} else {
		ssd1306_SetCursor ( 25, 0 );
		ssd1306_WriteString ( "SCOOTER INFO", &bahn12pt, White );
		ssd1306_drawHLine ( 0, 13, 128, White );

		snprintf ( buf, sizeof ( buf ), "S/N:   %s", local_data.S23C10.serial );
		ssd1306_SetCursor ( 0, 18 );
		ssd1306_WriteString ( buf, &bahn12pt, White );

		//		snprintf(buf, sizeof(buf), "FW:    %x", local_data.S23C10.version);
		snprintf ( buf, sizeof ( buf ), "FW:    %x   Mode: %x", local_data.S23C10.version, local_data.S21C00HZ64.state );
		ssd1306_SetCursor ( 0, 34 );
		ssd1306_WriteString ( buf, &bahn12pt, White );

		snprintf ( buf, sizeof ( buf ), "Temps:  %02d C  %02d C  %02d C",
				   local_data.S23CB0.mainframeTemp / 10,
				   local_data.S25C31.temp1 - 20, local_data.S25C31.temp2 - 20 );
		ssd1306_SetCursor ( 0, 50 );
		ssd1306_WriteString ( buf, &bahn12pt, White );

		switch ( button ) {
			case Brake_short:
				break;

			case Brake_long:
				screens.screen = 1;				//переходим на экран Trip
				screens.subscreen = 0;
				break;

			case Thr_long:
				screens.screen = 0;				//переходим на экран Main
				screens.subscreen = 0;
				break;

			case Both_long:
				screens.screen = 2;				//переходим на экран Menu
				screens.subscreen = 0;
				requestsSI[0].isEnabled = 1;
				break;

			default:
				break;
		}

		button = NONE;	//отработано
	}
}

/* Вызывается каждые 200мс */
void TIM4_IRQHandler()
{
	static M365_control_t control = { 0, 0 };

	if ( TIM_GetITStatus ( TIM4, TIM_IT_Update ) != RESET ) {
		TIM_ClearITPendingBit ( TIM4, TIM_IT_Update );

		if ( silentMode == 0 ) {
			/* обработка управления */
			if ( ( local_data.S21C00HZ64.state % 2 == 0 )  && ( local_data.S21C00HZ64.state < 5 ) ) { //0,2,4 - eco, drive, sport

				if ( local_data.S23CB0.speed > 0 ) {	//В движении
					if ( settings.ridescr != 0 ) {
						screens.screen = 7;
					} else {
						screens.screen = 0;
						screens.subscreen = 0;
					}
				} else  {				//Стоим
					if ( ( screens.screen >= 7 ) && ( local_data.S21C00HZ64.state < 12 ) ) {
						screens.screen = 0;
						screens.subscreen = 0;
					}

					if ( local_data.S20C00HZ65.brake >= 100 ) { //выжат тормоз
						control.brake++;
					} else if ( control.brake > 2 ) {
						button = Brake_short;
						control.brake = 0;
					}

					if ( local_data.S20C00HZ65.throttle >= 100 ) { //выжат газ
						control.throttle++;
					} else if ( control.throttle > 2 ) {	//короткое нажатие
						button = Thr_short;
						control.throttle = 0;
					}

					if ( ( control.throttle > SEC_1 ) && ( control.brake > SEC_1 ) ) { //Зажаты одновременно газ и тормоз более 2х секунд
						button = Both_long;
						control.throttle = 0;
						control.brake = 0;
					}

					if ( control.brake > SEC_2 ) {	//был зажат больше 2х секунд
						button = Brake_long;
						control.brake = 0;
					}

					if ( control.throttle > SEC_2 ) { //был зажат больше 2x секунд
						button = Thr_long;
						control.throttle = 0;
					}
				}
			} else { // Зарядка
				screens.screen = 8;
			}

			/* обработка максимальных величин */
			if ( trip_data.maxCurrent < local_data.S25C31.current )
			{ trip_data.maxCurrent = local_data.S25C31.current; }

			if ( trip_data.maxSpeed < local_data.S23CB0.speed )
			{ trip_data.maxSpeed = local_data.S23CB0.speed; }
		}
	}
}
