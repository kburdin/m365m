
#include "idog.h"
#include "data.h"
#include "stm32f10x_conf.h"

/*
 *	Data definition:
 */


/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		-

Prototype in	-

Description	-

*---------------------------------------------------------------------*/
void IWDG_Init(uint8_t prescaler, uint16_t value)
{
	/*Страница 485 мануала */
	IWDG->KR=0X5555;
	IWDG->PR=prescaler;
	IWDG->RLR=value;
	IWDG->KR=0XAAAA;
	IWDG->KR=0XCCCC;
}

/*---------------------------------------------------------------------*

Name		-

Usage		-

Prototype in	-

Description	-

*---------------------------------------------------------------------*/
void IWDG_Feed(void)
{
		IWDG->KR=0XAAAA;
}
