
// #include "stm32f10x_it.h"

#include "timeup.h"
#include "data.h"

/*
 *	Data definition:
 */

__IO uint32_t LocalTime = 0; /* this variable is used to create a time reference incremented by 10ms */

/*
 *	Function(s) definition:
 */


/*---------------------------------------------------------------------*

Name		- 

Usage		- 

Prototype in	- 

Description	- 

*---------------------------------------------------------------------*/
void Time_Update(void)
{
	LocalTime += SYSTEMTICK_PERIOD_MS;
}


/*---------------------------------------------------------------------*

Name		- 

Usage		- 

Prototype in	- 

Description	- 

*---------------------------------------------------------------------*/
void Delay(uint32_t nCount)
{
	/* Capture the current local time */
	static uint32_t timingdelay;

	timingdelay = LocalTime + nCount;

	/* wait until the desired delay finish */
	while (timingdelay > LocalTime) { }
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  /* Update the LocalTime by adding SYSTEMTICK_PERIOD_MS each SysTick interrupt */
  Time_Update();
}
