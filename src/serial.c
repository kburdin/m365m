﻿#include <stdio.h>
#include "serial.h"
#include "data.h"

Serial_Status_t SStatus[2];

static const uint32_t	DMA_USART[2] = {USART1_BASE, USART2_BASE};
static const uint32_t	RX_DMA_CHANNEL[2] = {DMA1_Channel5_BASE, DMA1_Channel6_BASE};
static const uint32_t	TX_DMA_CHANNEL[2] = {DMA1_Channel4_BASE, DMA1_Channel7_BASE};
static const uint32_t	RX_DMA_IFCR[2] = {
							DMA_IFCR_CTCIF5 | DMA_IFCR_CGIF5 | DMA_IFCR_CHTIF5 | DMA_IFCR_CTEIF5,
							DMA_IFCR_CTCIF6 | DMA_IFCR_CGIF6 | DMA_IFCR_CHTIF6 | DMA_IFCR_CTEIF6
						};
static const uint32_t	TX_DMA_IFCR[2] = {
							DMA_IFCR_CTCIF4 | DMA_IFCR_CGIF4 | DMA_IFCR_CHTIF4 | DMA_IFCR_CTEIF4,
							DMA_IFCR_CTCIF7 | DMA_IFCR_CGIF7 | DMA_IFCR_CHTIF7 | DMA_IFCR_CTEIF7
						};




void PrepareReceiveFromUSART(uint8_t usart, uint8_t *buff, uint32_t size)
{
	DMA_InitTypeDef	DMA_InitStruct;

	DMA_StructInit(&DMA_InitStruct);
	DMA_Cmd(((DMA_Channel_TypeDef *) RX_DMA_CHANNEL[usart]), DISABLE);			// отключаю DMA для получения доступа к регистрам
	DMA1->IFCR |= RX_DMA_IFCR[usart];											// очищу все флаги прерываний
	DMA_DeInit(((DMA_Channel_TypeDef *) RX_DMA_CHANNEL[usart]));				// на всякимй случай
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&(((USART_TypeDef *) DMA_USART[usart])->DR);	// источник - регистр данных UART
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)buff;							// источник - мой буфер (размер 256 байт)
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;								// направление из переферии в память
	DMA_InitStruct.DMA_BufferSize = size;										// размер принимающего буфера
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;				// адрес переферии не инкрементируется
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;						// адрес (ссылка на буфер) инкрементируется
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;		// размер данных переферии БАЙТ
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;				// размер данных буфера БАЙТ
	DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(((DMA_Channel_TypeDef *) RX_DMA_CHANNEL[usart]), &DMA_InitStruct);
	SStatus[usart].RxCount = 0;
	SStatus[usart].RxBufferSize = size;
}

void ReceiveFromUSART(uint8_t usart)
{
	((USART_TypeDef *) DMA_USART[usart])->CR1 |=	USART_CR1_RE;				// Включаем приемник
	DMA_Cmd(((DMA_Channel_TypeDef *) RX_DMA_CHANNEL[usart]), ENABLE);			// включаю DMA... и он начинает складывать поступающие данные в заданный буфер
}

void SendToUSART(uint8_t usart, const uint8_t *obuff, uint32_t osize)
{
	DMA_InitTypeDef	DMA_InitStruct;

	DMA_StructInit(&DMA_InitStruct);
	DMA_Cmd(((DMA_Channel_TypeDef *) TX_DMA_CHANNEL[usart]), DISABLE);			// отключаю DMA для получения доступа к регистрам
	DMA1->IFCR |= TX_DMA_IFCR[usart];											// очищу все флаги прерываний
	DMA_DeInit(((DMA_Channel_TypeDef *) TX_DMA_CHANNEL[usart]));				// на всякимй случай
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&(((USART_TypeDef *) DMA_USART[usart])->DR);	// источник - регистр данных UART
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t) obuff;						// источник - мой буфер (размер 256 байт)
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;								// направление из памяти в переферию
	DMA_InitStruct.DMA_BufferSize = osize;										// размер принимающего буфера
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;				// адрес переферии не инкрементируется
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;						// адрес (ссылка на буфер) инкрементируется
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;		// размер данных переферии БАЙТ
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;				// размер данных буфера БАЙТ
	DMA_Init(((DMA_Channel_TypeDef *) TX_DMA_CHANNEL[usart]), &DMA_InitStruct);

#ifdef DEBUG
	printf("S %ld b: ", osize);
	for (int i = 0; i < osize; i++)
		printf("%X ", obuff[i]);
	printf("\r\n");
#endif
	((USART_TypeDef *) DMA_USART[usart])->CR1 &= ~USART_CR1_RE;					// Отключаем приемник
	DMA_Cmd(((DMA_Channel_TypeDef *) TX_DMA_CHANNEL[usart]), ENABLE);
}

#ifdef USEUSART1
void USART1_IRQHandler(void)
{
	int status = USART1->SR;

	if (status & USART_SR_TC) 				// Передача окончена (последний байт полностью передан в порт)
	{
		USART1->SR &= ~USART_SR_TC; 		// Снимаем флаг

		USART1->CR1 |=	USART_CR1_RE;		// Включаем приемник
	}
	else if (status & USART_SR_IDLE) 		// Между байтами при приёме обнаружена пауза в 1 IDLE байт
	{
		SStatus[COM1].RxCount = SStatus[COM1].RxBufferSize - DMA1_Channel5->CNDTR;
		USART1->DR; 						// Снимаем флаг
		checkRXData();
	}
}
#endif
#ifdef USEUSART2
void USART2_IRQHandler(void)
{
	int status = USART2->SR;

	if (status & USART_SR_TC) // Передача окончена (последний байт полностью передан в порт)
	{
		USART2->SR &= ~USART_SR_TC; // Снимаем флаг
		_Query.prepared = 0;

		USART2->CR1 |=	USART_CR1_RE;	// Включаем приемник
	}
	else if (status & USART_SR_IDLE) // Между байтами при приёме обнаружена пауза в 1 IDLE байт
	{
		SStatus[COM2].RxCount = SStatus[COM2].RxBufferSize - DMA1_Channel6->CNDTR;
		USART2->DR; // Снимаем флаг
		checkRXData();
//		EXTI_GenerateSWInterrupt(EXTI_Line0);
	}
}
#endif
/*---------------------------------------------------------------------*

Name		- 

Usage		- 

Prototype in	- 

Description	- 

*---------------------------------------------------------------------*/
void PrepareReceiveCOM(uint8_t usart, uint8_t *buf, int length)
{
	PrepareReceiveFromUSART(usart, buf, length);
	ReceiveFromUSART(usart);
}

