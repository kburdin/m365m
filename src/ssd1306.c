#include <stdio.h>
#include "ssd1306.h"
#include "i2c.h"
#include "timeup.h"
#include "string.h"

/*
 **  Image data for image
 */
/*62x48*/
/*
const uint8_t testImage[] = { 0x00, 0x00, 0x00, 0x1F, 0xE0, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x03, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x80,
		0x07, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x78, 0x00, 0x00,
		0x00, 0x01, 0xE0, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x03, 0x80, 0x00,
		0x00, 0x07, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x01, 0xC0, 0x00,
		0x00, 0x1C, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x00, 0x00, 0x38, 0x00, 0x7F,
		0xF0, 0x00, 0x70, 0x00, 0x00, 0x60, 0x01, 0xFF, 0xF8, 0x00, 0x18, 0x00,
		0x00, 0xC0, 0x00, 0xC0, 0x38, 0x00, 0x0C, 0x00, 0x01, 0x80, 0x00, 0x00,
		0x00, 0x00, 0x06, 0x00, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00,
		0x03, 0x00, 0x80, 0x00, 0x00, 0x1C, 0x03, 0x00, 0x06, 0x01, 0xC0, 0x00,
		0x00, 0x3E, 0x01, 0x80, 0x0C, 0x03, 0x00, 0x00, 0x00, 0x1E, 0x00, 0xC0,
		0x0C, 0x06, 0x00, 0x00, 0x00, 0x0F, 0x00, 0xC0, 0x18, 0x06, 0x00, 0x00,
		0x00, 0x07, 0x80, 0x60, 0x18, 0x0C, 0x00, 0x00, 0x00, 0x03, 0xC0, 0x60,
		0x30, 0x18, 0x00, 0x00, 0x10, 0x03, 0xC0, 0x30, 0x30, 0x18, 0x00, 0x00,
		0x00, 0x01, 0xE0, 0x30, 0x20, 0x10, 0x00, 0x00, 0x20, 0x01, 0xE0, 0x10,
		0x60, 0x30, 0x00, 0x00, 0x20, 0x00, 0xF0, 0x18, 0x60, 0x20, 0x00, 0x00,
		0x60, 0x00, 0xF0, 0x18, 0x60, 0x20, 0x00, 0x00, 0xC0, 0x00, 0x70, 0x18,
		0x40, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x08, 0xC0, 0x00, 0x00, 0x01,
		0xC0, 0x00, 0x00, 0x0C, 0xC0, 0x00, 0x00, 0x03, 0xC0, 0x00, 0x00, 0x0C,
		0xC0, 0x00, 0x00, 0x03, 0x80, 0x00, 0x00, 0x0C, 0xC0, 0x00, 0x00, 0x07,
		0x80, 0x00, 0x00, 0x0C, 0xC0, 0x00, 0x00, 0x0F, 0x80, 0x00, 0x00, 0x0C,
		0xC0, 0x00, 0x00, 0x1F, 0x80, 0x00, 0x00, 0x0C, 0xC0, 0x40, 0x00, 0x3F,
		0x80, 0x00, 0x78, 0x0C, 0xC0, 0x20, 0x00, 0x3F, 0x80, 0x00, 0x70, 0x0C,
		0xC0, 0x20, 0x00, 0x3F, 0x80, 0x00, 0xF0, 0x0C, 0xC0, 0x20, 0x00, 0x1F,
		0x00, 0x00, 0xF0, 0x0C, 0xC0, 0x20, 0x00, 0x0E, 0x00, 0x00, 0xF0, 0x0C,
		0xC0, 0x10, 0x00, 0x00, 0x00, 0x01, 0xE0, 0x0C, 0x40, 0x10, 0x00, 0x00,
		0x00, 0x01, 0xE0, 0x08, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x18,
		0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x60, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x18, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10,
		0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x30, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60,
		0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x0F, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xC0, };

*/
// Screenbuffer
static uint8_t SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8];

// Screen object
static SSD1306_t SSD1306;

//
//  Send a byte to the command register
//
static void ssd1306_WriteCommand(uint8_t command) {
#define COMMAND_BYTE 0x00

	I2C_single_write(I2C1, SSD1306_I2C_ADDR, COMMAND_BYTE, command);
}

//
//	Initialize the oled screen
//
uint8_t ssd1306_Init(void) {
	// Wait for the screen to boot
	Delay(100);

	/* Init LCD */
	ssd1306_WriteCommand(0xAE); //display off
	ssd1306_WriteCommand(0x20); //Set Memory Addressing Mode
	ssd1306_WriteCommand(0x10); //00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
	ssd1306_WriteCommand(0xB0); //Set Page Start Address for Page Addressing Mode,0-7
	ssd1306_WriteCommand(0xC8); //Set COM Output Scan Direction
	ssd1306_WriteCommand(0x00); //---set low column address
	ssd1306_WriteCommand(0x10); //---set high column address
	ssd1306_WriteCommand(0x40); //--set start line address
	ssd1306_WriteCommand(0x81); //--set contrast control register
	ssd1306_WriteCommand(0xFF);
	ssd1306_WriteCommand(0xA1); //--set segment re-map 0 to 127
	ssd1306_WriteCommand(0xA6); //--set normal display
	ssd1306_WriteCommand(0xA8); //--set multiplex ratio(1 to 64)
	ssd1306_WriteCommand(0x3F); //
	ssd1306_WriteCommand(0xA4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content
	ssd1306_WriteCommand(0xD3); //-set display offset
	ssd1306_WriteCommand(0x00); //-not offset
	ssd1306_WriteCommand(0xD5); //--set display clock divide ratio/oscillator frequency
	ssd1306_WriteCommand(0xF0); //--set divide ratio
	ssd1306_WriteCommand(0xD9); //--set pre-charge period
	ssd1306_WriteCommand(0x22); //
	ssd1306_WriteCommand(0xDA); //--set com pins hardware configuration
	ssd1306_WriteCommand(0x12);
	ssd1306_WriteCommand(0xDB); //--set vcomh
	ssd1306_WriteCommand(0x20); //0x20,0.77xVcc
	ssd1306_WriteCommand(0x8D); //--set DC-DC enable
	ssd1306_WriteCommand(0x14); //
	ssd1306_WriteCommand(0xAF); //--turn on SSD1306 panel

	// Clear screen
	ssd1306_Fill(Black);

	// Flush buffer to screen
	ssd1306_UpdateScreen();

	// Set default values for screen object
	SSD1306.CurrentX = 0;
	SSD1306.CurrentY = 0;

	SSD1306.Initialized = 1;

	return 1;
}

//
//  Fill the whole screen with the given color
//
void ssd1306_Fill(SSD1306_COLOR color) {
	memset(SSD1306_Buffer, (color == Black) ? 0x00 : 0xFF,
	SSD1306_WIDTH * SSD1306_HEIGHT / 8);
}

//
//  Write the screenbuffer with changed to the screen
//
void ssd1306_UpdateScreen(void) {
	uint8_t i;

	for (i = 0; i < 8; i++) {
		ssd1306_WriteCommand(0xB0 + i);
		ssd1306_WriteCommand(0x00);
		ssd1306_WriteCommand(0x10);

		I2C_burst_write(I2C1, SSD1306_I2C_ADDR, 0x40, SSD1306_WIDTH,
				&SSD1306_Buffer[SSD1306_WIDTH * i]);
	}
}

//
//	Draw one pixel in the screenbuffer
//	X => X Coordinate
//	Y => Y Coordinate
//	color => Pixel color
//
void ssd1306_DrawPixel(uint8_t x, uint8_t y, SSD1306_COLOR color) {
	if (x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT) {
		// Don't write outside the buffer
		return;
	}

	// Check if pixel should be inverted
	if (SSD1306.Inverted) {
		color = (SSD1306_COLOR) !color;
	}

	// Draw in the right color
	if (color == White) {
		SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] |= 1 << (y % 8);
	} else {
		SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] &= ~(1 << (y % 8));
	}
}

//
//  Draw 1 char to the screen buffer
//	ch 		=> char om weg te schrijven
//	Font 	=> Font waarmee we gaan schrijven
//	color 	=> Black or White
//
char ssd1306_WriteChar(char ch, const FONT_INFO* Font, SSD1306_COLOR color) {
	uint8_t width, height, b;
	uint32_t c, i, j;
	const uint8_t *cp;
	if (ch < Font->start_char || ch > Font->end_char)
		return 0;

	c = ch - Font->start_char;
	width = Font->p_character_descriptor[c].width + 1;
	height = Font->height;
	if (width > 1) {
		if (SSD1306_WIDTH <= (SSD1306.CurrentX + width)
				|| SSD1306_HEIGHT <= (SSD1306.CurrentY + height))
			return 0;
		cp = &Font->p_character_bitmaps[Font->p_character_descriptor[c].offset];
		for (i = 0; i < height; i++) {
			b = *cp++;
			for (j = 0; j < width - 1; j++) {
				if ((j > 0) && (j % 8 == 0)) {
					b = *cp;
					cp++;
				}
				if ((b << (j % 8)) & 0x80) {
					ssd1306_DrawPixel(SSD1306.CurrentX + j,
							(SSD1306.CurrentY + i), (SSD1306_COLOR) color);
				} else {
					ssd1306_DrawPixel(SSD1306.CurrentX + j,
							(SSD1306.CurrentY + i), (SSD1306_COLOR) !color);
				}
			}
			ssd1306_DrawPixel(SSD1306.CurrentX + j,	(SSD1306.CurrentY + i), (SSD1306_COLOR) !color);

		}
		SSD1306.CurrentX += width;
	} else
		return 0;
	return ch;
}
char ssd1306_WriteString(char* str, const FONT_INFO* Font, SSD1306_COLOR color) {
	// Write until null-byte
	while (*str) {
		if (ssd1306_WriteChar(*str, Font, color) != *str) {
			// Char could not be written
			return *str;
		}

		// Next char
		str++;
	}

	// Everything ok
	return *str;
}
//
//	Position the cursor
//
void ssd1306_SetCursor(uint8_t x, uint8_t y) {
#ifdef SH1106
	x = x + 2;
#endif
	SSD1306.CurrentX = x;
	SSD1306.CurrentY = y;
}

void ssd1306_drawVLine(uint8_t x, uint8_t y, uint8_t h, SSD1306_COLOR color) {
#ifdef SH1106
	x = x + 2;
#endif
	if ((x <= SSD1306_WIDTH) && (h <= SSD1306_HEIGHT)) {
		if ((y + h) > SSD1306_HEIGHT) {
			h = (SSD1306_HEIGHT - y);
		}
		if (h > 0) {
			uint8_t dy = y, dh = h;
			uint8_t *pBuf = &SSD1306_Buffer[(y / 8) * SSD1306_WIDTH + x];

			// do the first partial byte, if necessary - this requires some masking
			uint8_t mod = (dy & 7);
			if (mod) {
				// mask off the high n bits we want to set
				mod = 8 - mod;
				// note - lookup table results in a nearly 10% performance
				// improvement in fill* functions
				// uint8_t mask = ~(0xFF >> mod);
				static const uint8_t premask[8] = { 0x00, 0x80, 0xC0, 0xE0,
						0xF0, 0xF8, 0xFC, 0xFE };
				uint8_t mask = *(&premask[mod]);
				// adjust the mask if we're not going to reach the end of this byte
				if (dh < mod)
					mask &= (0XFF >> (mod - dh));

				switch (color) {
				case White:
					*pBuf |= mask;
					break;
				case Black:
					*pBuf &= ~mask;
					break;
				}
				pBuf += SSD1306_WIDTH;
			}

			if (dh >= mod) { // More to go?
				dh -= mod;
				// Write solid bytes while we can - effectively 8 rows at a time
				if (dh >= 8) {
					// store a local value to work with
					uint8_t val = (color != Black) ? 255 : 0;
					do {
						*pBuf = val;    // Set byte
						pBuf += SSD1306_WIDTH;  // Advance pointer 8 rows
						dh -= 8;      // Subtract 8 rows from height
					} while (dh >= 8);
				}

				if (dh) { // Do the final partial byte, if necessary
					mod = dh & 7;
					// this time we want to mask the low bits of the byte,
					// vs the high bits we did above
					// uint8_t mask = (1 << mod) - 1;
					// note - lookup table results in a nearly 10% performance
					// improvement in fill* functions
					static const uint8_t postmask[8] = { 0x00, 0x01, 0x03, 0x07,
							0x0F, 0x1F, 0x3F, 0x7F };
					uint8_t mask = *(&postmask[mod]);
					switch (color) {
					case White:
						*pBuf |= mask;
						break;
					case Black:
						*pBuf &= ~mask;
						break;
					}
				}
			}
		} // endif positive height
	} // endif x in bounds
}

void ssd1306_drawHLine(uint8_t x, uint8_t y, uint8_t w, SSD1306_COLOR color) {
#ifdef SH1106
	x = x + 2;
#endif
	if ((y <= SSD1306_HEIGHT) && (w <= SSD1306_WIDTH)) {

		if ((x + w) > SSD1306_WIDTH) {
			w = (SSD1306_WIDTH - x);
		}
		if (w > 0) {
			uint8_t *pBuf = &SSD1306_Buffer[(y / 8) * SSD1306_WIDTH + x], mask =
					1 << (y & 7);
			switch (color) {
			case White:
				while (w--) {
					*pBuf++ |= mask;
				}
				;
				break;
			case Black:
				mask = ~mask;
				while (w--) {
					*pBuf++ &= mask;
				}
				;
				break;
			}
		}
	}
}

void ssd1306_drawRectangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, SSD1306_COLOR color) {
	ssd1306_drawHLine(x1, y1, x2 - x1, color);
	ssd1306_drawHLine(x1, y2, x2 - x1, color);
	ssd1306_drawVLine(x1, y1, y2 - y1, color);
	ssd1306_drawVLine(x2, y1, y2 - y1, color);
}

void ssd1306_DrawBitmap(const uint8_t *data, uint8_t w, uint8_t h, SSD1306_COLOR color) {
	uint32_t i, j;
	uint8_t  b;
	const uint8_t *cp;

	if (SSD1306_WIDTH < (SSD1306.CurrentX + w) || SSD1306_HEIGHT < (SSD1306.CurrentY + h))
		return;
	cp = data;
	for (i = 0; i < h; i++) {
		b = *cp++;
		for (j = 0; j < w; j++) {
			if ((j > 0) && (j % 8 == 0)) {
				b = *cp;
				cp++;
			}
			if ((b << (j % 8)) & 0x80) {
				ssd1306_DrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i),
						(SSD1306_COLOR) !color);
			} else {
				ssd1306_DrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i),
						(SSD1306_COLOR) color);
			}
		}
	}

	SSD1306.CurrentX += w;
}
/*
void ssd1306_TestFPS(const FONT_INFO* Font) {

	ssd1306_Fill(Black);

	uint32_t start = LocalTime;
	uint32_t end = LocalTime;
	int fps = 0;
	char message[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	ssd1306_SetCursor(0, 0);
	ssd1306_WriteString("Testing...", Font, White);

	do {
		ssd1306_SetCursor(0, 32);
		ssd1306_WriteString(message, Font, White);
		ssd1306_UpdateScreen();

		char ch = message[0];
		memmove(message, message + 1, sizeof(message) - 2);
		message[sizeof(message) - 2] = ch;

		fps++;
		end = LocalTime;
	} while ((end - start) < 500);

	ssd1306_Fill(Black);
	ssd1306_drawVLine(2, 30, 30, White);
	ssd1306_drawVLine(12, 30, 30, White);
	ssd1306_drawVLine(22, 30, 30, White);
	ssd1306_drawVLine(32, 30, 30, White);
	ssd1306_drawHLine(2, 30, 30, White);
	ssd1306_drawHLine(2, 40, 30, White);
	ssd1306_drawHLine(2, 50, 30, White);
	ssd1306_drawHLine(2, 60, 30, White);
	ssd1306_drawRectangle(0, 0, 127, 63, White);
	ssd1306_SetCursor(38, 10);
	ssd1306_DrawBitmap(testImage, 62, 48, Black);

	char buff[64];
	fps = (float) fps / ((end - start) / 1000.0);
	snprintf(buff, sizeof(buff), "~%d FPS", fps);
	ssd1306_SetCursor(2, 10);
	ssd1306_WriteString(buff, Font, White);

	ssd1306_UpdateScreen();
}

*/
