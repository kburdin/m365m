#include "data.h"
#include "serial.h"
#include "display.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/*
 *	Data definition:
 */
extern uint8_t buff[DMA_BUFFER_SIZE];
extern uint8_t silentMode;
extern screens_t screens;

local_data_t local_data;
trip_data_t trip_data = { 0, 0, 0, 0 };

/*
 * 55aa 03 20 01 3a 04 9dff		//Пробеги
 * 55aa 03 20 01 b0 20 0bff		//общая инф.
 * 55aa 03 20 01 25 02 b4ff		//остаточный пробег
 * 55aa 03 22 01 31 0a 9eff  		//Данные от батареи
 *
 * 55aa 03 22 01 40 1e 7bff		//Информация по ячейкам
 * 55aa 03 22 01 10 12 b7ff		//серийник батареи, емкость, версия BMS
 * 55aa 03 22 01 20 02 b7ff		//дата производства 16 бит 7 MSB->año, siguientes 4bits->mes, 5 LSB ->dia
 * 55aa 03 22 01 1b 04 baff		//циклы заряда
 */

requests_t requestsMT[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0x3A, 0x04, 0x9D, 0xFF }, 3 },	//
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0xB0, 0x20, 0x0B, 0xFF }, 3 }, 	//
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0x25, 0x02, 0xB4, 0xFF }, 3 }, 	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x31, 0x0A, 0x9E, 0xFF }, 3 } 	//
};

requests_t requestsRD[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0xB0, 0x20, 0x0B, 0xFF }, 3 }, 	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x31, 0x0A, 0x9E, 0xFF }, 3 } 	//
};


requests_t requestsB[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x31, 0x0A, 0x9E, 0xFF }, 3 },
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x40, 0x1e, 0x7b, 0xFF }, 3 },	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x10, 0x12, 0xb7, 0xFF }, 3 },	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x20, 0x02, 0xb7, 0xFF }, 3 },	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x1b, 0x04, 0xba, 0xFF }, 3 },	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x3b, 0x02, 0x9C, 0xFF }, 3 }		//
};

requests_t requestsCH[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x31, 0x0A, 0x9E, 0xFF }, 3 }, 	//
	{ { 0x55, 0xAA, 0x03, 0x22, 0x01, 0x40, 0x1e, 0x7b, 0xFF }, 3 }	//
};

requests_t requestsSI[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0x10, 0x16, 0xB5, 0xFF }, 3 } 	//
};

requests_t requestsST[] = {
	/*         | HEADER  | LEN | ADDR| CMD | REG | CNT |   CRC     | */
	{ { 0x55, 0xAA, 0x03, 0x20, 0x01, 0x7B, 0x06, 0x5A, 0xFF }, 3 }
};

commands_t commands[] = {
	/*         | HEADER  |  LEN | ADDR| CMD | REG |   VALUE   |   CRC     | */
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7B, 0x00, 0x00, 0x5D, 0xFF }, 0 },	// KERS low
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7B, 0x01, 0x00, 0x5C, 0xFF }, 0 },	// KERS medium
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7B, 0x02, 0x00, 0x5B, 0xFF }, 0 },	// KERS high
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7C, 0x00, 0x00, 0x5C, 0xFF }, 0 },	// Cruise off
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7C, 0x01, 0x00, 0x5B, 0xFF }, 0 },	// Cruise on
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7D, 0x00, 0x00, 0x5B, 0xFF }, 0 },	// Tail LED off
	{ { 0x55, 0xAA, 0x04, 0x20, 0x03, 0x7D, 0x02, 0x00, 0x59, 0xFF }, 0 }	// Tail LED on
};

void *request[] = { &requestsMT, &requestsMT, NULL, &requestsB, &requestsSI, &requestsST, NULL,  &requestsRD, &requestsCH};	//MainScreen, TripScreen, MenuScreen, BattScreen, SInfoScreen, SetupScreen, SettingsScreen
uint8_t reqcnt[] = { sizeof ( requestsMT ) / sizeof ( requests_t ), sizeof ( requestsMT ) / sizeof ( requests_t ), 0, sizeof ( requestsB ) / sizeof ( requests_t ), sizeof ( requestsSI ) / sizeof ( requests_t ), sizeof ( requestsST ) / sizeof ( requests_t ), 0, sizeof ( requestsRD ) / sizeof ( requests_t ), sizeof ( requestsCH ) / sizeof ( requests_t ) };

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

 Name		-

 Usage		-

 Prototype in	-

 Description	-

 *---------------------------------------------------------------------*/
void sendRequest ( void )
{
	static uint8_t idx = 0;

	for ( int i = 0; i < sizeof ( commands ) / sizeof ( commands_t ); i++ ) {
		if ( commands[i].isEnabled ) {
			SendToUSART ( DEFCOM, commands[i].data, 10 );
			commands[i].isEnabled = 0;
			return;
		}
	}

	requests_t *req = request[screens.screen];

	if ( req ) {
		idx %= reqcnt[screens.screen];

		if ( req[idx].isEnabled )
		{ SendToUSART ( DEFCOM, req[idx].data, 9 ); }

		idx++;
	}

}

/*---------------------------------------------------------------------*

 Name		-

 Usage		-

 Prototype in	-

 Description	-

 *---------------------------------------------------------------------*/

void processPacket ( packet_t *packet )
{
	uint8_t RawDataLen;

	RawDataLen = packet->head.len - 2;

	switch ( packet->head.addr ) { //store data into each other structure
		case 0x20: //0x20 - master to M365
			switch ( packet->head.cmd ) {
				case 0x00:
					switch ( packet->head.func ) {
						case 0x65:
							memcpy ( ( void * ) &local_data.S20C00HZ65, ( void * ) &packet->data.S20C00HZ65, sizeof ( A20C00HZ65 ) );

							if ( !silentMode )
							{ sendRequest(); }

							break;

						default: //no suitable func
							break;
					}

					break;

				default: //no suitable cmd
					break;
			}

			break;

		case 0x21:
			switch ( packet->head.cmd ) {
				case 0x00:
					switch ( packet->head.func ) {
						case 0x64: //answer to BLE
							if ( RawDataLen <= sizeof ( A21C00HZ64 ) )
							{ memcpy ( ( void * ) &local_data.S21C00HZ64, ( void * ) &packet->data.S21C00HZ64, RawDataLen ); }

							break;
					}

					break;

				default:
					break;
			}

			break;

		case 0x22:
			switch ( packet->head.cmd ) {
				default:
					break;
			}

			break;

		case 0x23:	//M365_TO_MASTER
			switch ( packet->head.cmd ) {
				case 0x10: //mainframe temperature
					if ( RawDataLen == sizeof ( A23C10 ) ) {
						memcpy ( ( void * ) &local_data.S23C10, ( void * ) &packet->data.S23C10, sizeof ( A23C10 ) );

						if ( requestsSI[0].isEnabled ) {
							requestsSI[0].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				case 0x7B: //mainframe temperature
					if ( RawDataLen == sizeof ( A23C7B ) ) {
						memcpy ( ( void * ) &local_data.S23C7B, ( void * ) &packet->data.S23C10, sizeof ( A23C7B ) );

						if ( requestsST[0].isEnabled ) {
							requestsST[0].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				case 0x3E: //mainframe temperature
					if ( RawDataLen == sizeof ( A23C3E ) )
					{ memcpy ( ( void * ) &local_data.S23C3E, ( void * ) &packet->data.S23C3E, sizeof ( A23C3E ) ); }

					break;

				case 0xB0: //speed, average speed, mileage total, mileage current, power on time, mainframe temp
					if ( RawDataLen == sizeof ( A23CB0 ) )
					{ memcpy ( ( void * ) &local_data.S23CB0, ( void * ) &packet->data.S23CB0, sizeof ( A23CB0 ) ); }

					break;

				case 0x25: //remain mileage
					if ( RawDataLen == sizeof ( A23C25 ) )
					{ memcpy ( ( void * ) &local_data.S23C25, ( void * ) &packet->data.S23C25, sizeof ( A23C25 ) ); }

					break;

				case 0x3A: //power on time, riding time
					if ( RawDataLen == sizeof ( A23C3A ) )
					{ memcpy ( ( void * ) &local_data.S23C3A, ( void * ) &packet->data.S23C3A, sizeof ( A23C3A ) ); }

					break;

				default:
					break;
			}

			break;

		case 0x25:	//BATTERY to MASTER
			switch ( packet->head.cmd ) {
				case 0x10:
					if ( RawDataLen == sizeof ( A25C10 ) ) {
						memcpy ( ( void * ) &local_data.S25C10, ( void * ) &packet->data.S25C10, sizeof ( A25C10 ) );

						if ( requestsB[1].isEnabled ) {
							requestsB[1].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				case 0x20:
					if ( RawDataLen == sizeof ( A25C20 ) ) {
						memcpy ( ( void * ) &local_data.S25C20, ( void * ) &packet->data.S25C20, sizeof ( A25C20 ) );

						if ( requestsB[2].isEnabled ) {
							requestsB[2].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				case 0x1b:
					if ( RawDataLen == sizeof ( A25C1B ) ) {
						memcpy ( ( void * ) &local_data.S25C1B, ( void * ) &packet->data.S25C1B, sizeof ( A25C1B ) );

						if ( requestsB[3].isEnabled ) {
							requestsB[3].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				case 0x40: //cells info
					if ( RawDataLen == sizeof ( A25C40 ) )
					{ memcpy ( ( void * ) &local_data.S25C40, ( void * ) &packet->data.S25C40, sizeof ( A25C40 ) ); }

					break;

				case 0x31: //capacity, remain percent, current, voltage
					if ( RawDataLen == sizeof ( A25C31 ) )
					{ memcpy ( ( void * ) &local_data.S25C31, ( void * ) &packet->data.S25C31, sizeof ( A25C31 ) ); }

					break;

				case 0x3b: //status
					if ( RawDataLen == sizeof ( A25C3B ) ) {
						memcpy ( ( void * ) &local_data.S25C3B, ( void * ) &packet->data.S25C3B, sizeof ( A25C3B ) );

						if ( requestsB[4].isEnabled ) {
							requestsB[4].isEnabled--; //Эти данные требуется получить только 1 раз...
						}
					}

					break;

				default:
					break;
			}

			break;

		default:
			break;
	}
}

/*---------------------------------------------------------------------*

 Name		-

 Usage		-

 Prototype in	-

 Description	-

 *---------------------------------------------------------------------*/
uint16_t computeCRC16 ( uint8_t *buf, uint16_t len )
{
	uint16_t checksum = 0xFFFF;

	for ( int i = 0; i < len; i++ )
	{ checksum -= buf[i]; }

	return checksum;
}
/*---------------------------------------------------------------------*

 Name		-

 Usage		-

 Prototype in	-

 Description	-

 *---------------------------------------------------------------------*/
void checkRXData ( void )
{
	static size_t head;
	size_t tail = SStatus[DEFCOM].RxCount;
	int16_t size;
	static uint16_t checksum = 0xFFFF;
	packet_t *packet = NULL;
	uint8_t isOverflow = 0;

	if ( head != tail ) {
		size = tail - head;

		if ( size < 0 ) {
			size += SStatus[DEFCOM].RxBufferSize;
			isOverflow = 1;
		}

#ifdef DEBUG
		printf ( "R %d b: ", size );

		for ( int i = 0; i < size; i++ )
		{ printf ( "%X ", buff[ ( head + i ) % SStatus[DEFCOM].RxBufferSize] ); }

		//		printf("\r\n");
#endif

		if ( size >= 9 )
			while ( head != tail - 2 ) {
				if ( buff[ ( head + Header0Pos ) % SStatus[DEFCOM].RxBufferSize] == Header0 ) {
					if ( buff[ ( head + Header1Pos ) % SStatus[DEFCOM].RxBufferSize] == Header1 ) {
						if ( buff[ ( head + LenPos ) % SStatus[DEFCOM].RxBufferSize] >= 3 ) { //Проверка размера и заголовка
							if ( ( buff[ ( head + LenPos ) % SStatus[DEFCOM].RxBufferSize] + 6 ) <= size ) { // Длина совпадает. Считаем CRC.
								if ( isOverflow ) {
									packet = malloc ( buff[ ( head + LenPos ) % SStatus[DEFCOM].RxBufferSize] + 6 );

									for ( int i = 0; i < ( buff[ ( head + LenPos ) % SStatus[DEFCOM].RxBufferSize] + 6 ); i++ )
									{ ( ( char * ) packet ) [i] = buff[ ( head + i ) % SStatus[DEFCOM].RxBufferSize]; }

									//TODO: Где-то здесь косяк:
									//										memcpy(packet, (void *) (buff + head), SStatus[DEFCOM].RxBufferSize - head); //Копируем от начала пакета до конца буфера.
									//										memcpy((void *) packet + (SStatus[DEFCOM].RxBufferSize - head), (void *) buff, (buff[(head + LenPos) % SStatus[DEFCOM].RxBufferSize] + 6) - (SStatus[DEFCOM].RxBufferSize - head)); //Копируем оставшуюся часть
								} else
								{ packet = ( packet_t * ) ( buff + head ); }

								//TODO: В STM32 кажется был свой модуль расчета CRC. посмотреть.
								checksum = 0xFFFF;

								for ( int i = 0; i < sizeof ( ANSWER_HEADER ) + packet->head.len - CRCLEN - INITLEN; i++ )
								{ checksum -= ( ( uint8_t * ) packet ) [i + 2]; }

								uint16_t *ipcs;
								ipcs = ( uint16_t * ) ( packet->data.buf + packet->head.len - 2 );

								if ( *ipcs == checksum ) { //check cs
#ifdef DEBUG
									printf ( "Good\r\n" );
#endif
#ifdef DEBUG

									//		printf("R %d b: ", size);
									for ( int i = 0; i < packet->head.len + 6; i++ )
									{ printf ( "%X ", ( ( char * ) packet ) [i] ); }

									printf ( "\r\n" );
#endif
									processPacket ( packet );
								}

#ifdef DEBUG
								else
								{ printf ( "Wrong checksum\r\n" ); }

#endif

								if ( isOverflow )
								{ free ( packet ); }

								head = ( head + buff[ ( head + LenPos ) % SStatus[DEFCOM].RxBufferSize] + 6 ) % SStatus[DEFCOM].RxBufferSize;
							} else
							{ return; } //Ждем еще больше байт

							break;
						}
					}
				}

				head++;
			}

		if ( head == SStatus[DEFCOM].RxBufferSize )
		{ head = 0; }
	}

}

