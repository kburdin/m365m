﻿/**
 ******************************************************************************
 * @file    stm32f107.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    11/20/2009
 * @brief   STM32F107 hardware configuration
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_conf.h"
#include "stm32f103.h"
#include "string.h"
#include "i2c.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void GPIO_Configuration ( void );
void NVIC_Configuration ( void );

#ifdef USEUSART1
	void USART1_Configuration ( void );
#endif

#ifdef USEUSART2
	void USART2_Configuration ( void );
#endif

void Periodic_Timer_Configuration ( void );

/**
 * @brief  Setup STM32 system (clocks, Ethernet, GPIO, NVIC) and resources.
 * @param  None
 * @retval None
 */
void System_Setup ( void )
{
	RCC_ClocksTypeDef RCC_Clocks;

	/* Setup STM32 clock, PLL and Flash configuration) */
	SystemInit();

	/* Enable GPIOs clocks */
	RCC_APB2PeriphClockCmd (
		RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC
		| RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );

	/* Configure the GPIO ports */
	GPIO_Configuration();

	/* Enable DMA clock */
	RCC_AHBPeriphClockCmd ( RCC_AHBPeriph_DMA1, ENABLE );

	RCC_APB1PeriphClockCmd ( RCC_APB1Periph_TIM4, ENABLE );
#ifdef USEUSART1
	/* Enable USART1 clock */
	RCC_APB2PeriphClockCmd ( RCC_APB2Periph_USART1, ENABLE );
	/* Configure USART1 peripheral */
	USART1_Configuration();
#endif

#ifdef USEUSART2
	/* Enable USART2 clock */
	RCC_APB1PeriphClockCmd ( RCC_APB1Periph_USART2, ENABLE );
	/* Configure USART2 peripheral */
	USART2_Configuration();
#endif

	/* NVIC configuration */
	NVIC_Configuration();

	Periodic_Timer_Configuration();

	I2C_init ( I2C1, 400000 );

	/* SystTick configuration: an interrupt every 10ms */
	RCC_GetClocksFreq ( &RCC_Clocks );
	SysTick_Config ( RCC_Clocks.SYSCLK_Frequency / 100 );

	NVIC_SetPriority ( SysTick_IRQn, 1 );
}

#ifdef USEUSART1
/**
 * @brief  Configures the USART1 Interface
 * @param  None
 * @retval None
 */
void USART1_Configuration ( void )
{
	USART_InitTypeDef USART_InitStruct;

	USART_StructInit ( &USART_InitStruct );
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_HalfDuplexCmd ( USART1, ENABLE );
	USART_Init ( USART1, &USART_InitStruct );

	USART_DMACmd ( USART1, USART_DMAReq_Rx, ENABLE );
	USART_DMACmd ( USART1, USART_DMAReq_Tx, ENABLE );

	USART1->CR1 = USART_CR1_RE;							//разрешить приЄмник
	USART1->CR1 |= USART_CR1_TE;						//разрешить передатчик
	USART1->CR1 |= USART_CR1_IDLEIE;					//разр. прерывани¤ по приЄму "паузе" между посылаемыми байтами
	USART1->CR1 |= USART_CR1_TCIE;						//¬ключение прерывани¤ по окончании передачи.
	USART1->CR1 |= USART_CR1_UE;						//разрешить UART1
}
#endif

#ifdef USEUSART2
/**
 * @brief  Configures the USART2 Interface
 * @param  None
 * @retval None
 */
void USART2_Configuration ( void )
{
	USART_InitTypeDef USART_InitStruct;

	USART_StructInit ( &USART_InitStruct );
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	//	USART_HalfDuplexCmd(USART2, ENABLE);
	USART_Init ( USART2, &USART_InitStruct );

	USART_DMACmd ( USART2, USART_DMAReq_Rx, ENABLE );
	USART_DMACmd ( USART2, USART_DMAReq_Tx, ENABLE );

	USART2->CR1 = USART_CR1_RE;							//разрешить приЄмник
	USART2->CR1 |= USART_CR1_TE;						//разрешить передатчик
	USART2->CR1 |= USART_CR1_IDLEIE;					//разр. прерывани¤ по приЄму "паузе" между посылаемыми байтами
	USART2->CR1 |= USART_CR1_TCIE;						//¬ключение прерывани¤ по окончании передачи.
	USART2->CR1 |= USART_CR1_UE;						//разрешить UART2
}
#endif

/**
 * @brief  Configures the different GPIO ports.
 * @param  None
 * @retval None
 */
void GPIO_Configuration ( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
#ifdef USEUSART1
	/* USART1 configuration (Single wire)*/
	GPIO_StructInit ( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init ( GPIOA, &GPIO_InitStructure );
	// Full duplex for debug purposes
	///*	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	//	GPIO_Init(GPIOA, &GPIO_InitStructure);*/
#endif
#ifdef USEUSART2
	/* USART2 configuration Async mode*/
	//	GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
	GPIO_StructInit ( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init ( GPIOA, &GPIO_InitStructure );
	// Full duplex for debug purposes
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init ( GPIOA, &GPIO_InitStructure );
#endif

	GPIO_StructInit ( &GPIO_InitStructure );
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init ( GPIOB, &GPIO_InitStructure );
}

/**
 * @brief  Configures the nested vectored interrupt controller.
 * @param  None
 * @retval None
 */
void NVIC_Configuration ( void )
{
	NVIC_InitTypeDef NVIC_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;

	/* Set the Vector Table base location at 0x08000000 */
	NVIC_SetVectorTable ( NVIC_VectTab_FLASH, 0x0 );

	/* 2 bit for pre-emption priority, 2 bits for subpriority */
	NVIC_PriorityGroupConfig ( NVIC_PriorityGroup_2 );

#ifdef USEUSART1
	/* Enable the USART1 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init ( &NVIC_InitStructure );
#endif
#ifdef USEUSART2
	/* Enable the USART2 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init ( &NVIC_InitStructure );
#endif

	/* Configure EXTI Line0 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init ( &EXTI_InitStructure );

	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init ( &NVIC_InitStructure );
	//	NVIC_EnableIRQ(EXTI0_IRQn);

	/* Display timer */
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init ( &NVIC_InitStructure );
}

void Periodic_Timer_Configuration ( void )
{
	TIM_TimeBaseInitTypeDef TIM_InitStructure;

	TIM_TimeBaseStructInit ( &TIM_InitStructure );
	TIM_InitStructure.TIM_Prescaler = 7200 - 1; 		// частота на шине - 72 КГц. 72 000 000 / 7200 = 10 000
	TIM_InitStructure.TIM_Period = 2000;          		// период таймера
	TIM_TimeBaseInit ( TIM4, &TIM_InitStructure );

	NVIC_EnableIRQ ( TIM4_IRQn );
	TIM_ITConfig ( TIM4, TIM_DIER_UIE, ENABLE );
	//	TIM_Cmd(TIM4, ENABLE);
}

