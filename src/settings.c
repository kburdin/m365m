#include <string.h>
#include "settings.h"
#include "data.h"

extern settings_t settings;

void write_settings ( uint32_t addr, uint16_t *pBuffer, uint16_t nb )
{
	int i;

	FLASH_Unlock();
	FLASH_ErasePage ( addr );

	for ( i = 0; i < nb; i++ ) {
		FLASH_ProgramHalfWord ( addr, pBuffer[i] );
		addr += 2;
	}

	FLASH_Lock();
}

/*---------------------------------------------------------------------*

Name		-

Usage		-

Prototype in	-

Description	-

*---------------------------------------------------------------------*/
void restore_settings ( void )
{
	settings.ridescr = 0;
	settings.calcld = 0;
	settings.wheelmult = 1.0;
	settings.light = 0;
	save_settings();
}
/*---------------------------------------------------------------------*

Name		-

Usage		-

Prototype in	-

Description	-

*---------------------------------------------------------------------*/
void load_settings ( void )
{
	uint16_t crc;
	memcpy ( &settings, ( const void * ) Settings_BASE, sizeof ( settings_t ) );
	crc = computeCRC16 ( ( uint8_t * ) &settings, sizeof ( settings_t ) - 2 );

	if ( crc != settings.crc )
	{ restore_settings(); }
}

/*---------------------------------------------------------------------*

Name		-

Usage		-

Prototype in	-

Description	-

*---------------------------------------------------------------------*/
void save_settings ( void )
{
	settings.crc = computeCRC16 ( ( uint8_t * ) &settings, sizeof ( settings_t ) - 2 );
	write_settings ( Settings_BASE, ( uint16_t * ) &settings, sizeof ( settings_t ) / 2 );
}


