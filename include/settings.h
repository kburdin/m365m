#ifndef __SETTINGS_H
#define __SETTINGS_H

#include "stm32f10x_conf.h"
#define Settings_BASE (FLASH_BASE + 2048 * 31)

#ifdef __cplusplus
 extern "C" {
#endif

void write_settings(uint32_t, uint16_t *, uint16_t);
void load_settings(void);
void restore_settings(void);
void save_settings(void);

#ifdef __cplusplus
}
#endif

#endif
