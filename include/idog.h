#ifndef _IDOG_H
#define _IDOG_H

#include <stdint.h>

/*
 *	Macros definition
 */

/*
 *	Type declaration
 */

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void IWDG_Init(uint8_t, uint16_t);

void IWDG_Feed(void);

#ifdef __cplusplus
}
#endif

#endif
