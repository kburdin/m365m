#ifndef ssd1306
#define ssd1306

#include "stm32f10x_conf.h"
#include <fonts/font.h>


// I2c address
#define SSD1306_I2C_ADDR        0x78
// SSD1306 width in pixels
#ifdef SH1106
#define SSD1306_WIDTH           130
#else
#define SSD1306_WIDTH           128
#endif
// SSD1306 LCD height in pixels
#define SSD1306_HEIGHT          64
#define I2C_TIMEOUT             100000



//
//  Enumeration for screen colors
//
typedef enum {
	Black = 0x00, // Black color, no pixel
	White = 0x01  //Pixel is set. Color depends on LCD
} SSD1306_COLOR;

//
//  Struct to store transformations
//
typedef struct {
	uint16_t CurrentX;
	uint16_t CurrentY;
	uint8_t Inverted;
	uint8_t Initialized;
} SSD1306_t;


//
//  Function definitions
//
uint8_t ssd1306_Init(void);
void ssd1306_Fill(SSD1306_COLOR color);
void ssd1306_UpdateScreen(void);
void ssd1306_DrawPixel(uint8_t x, uint8_t y, SSD1306_COLOR color);
char ssd1306_WriteChar(char ch, const FONT_INFO* Font, SSD1306_COLOR color);
char ssd1306_WriteString(char* str, const FONT_INFO* Font, SSD1306_COLOR color);
void ssd1306_SetCursor(uint8_t x, uint8_t y);
void ssd1306_drawVLine( uint8_t x, uint8_t y, uint8_t h, SSD1306_COLOR color);
void ssd1306_drawHLine( uint8_t x, uint8_t y, uint8_t h, SSD1306_COLOR color);
void ssd1306_drawRectangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, SSD1306_COLOR color);
void ssd1306_DrawBitmap(const uint8_t *data, uint8_t w, uint8_t h, SSD1306_COLOR color);
void ssd1306_TestFPS(const FONT_INFO* Font);
#endif
