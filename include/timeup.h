#ifndef _TIME_UPDATE_H
#define _TIME_UPDATE_H

#include "stm32f10x_conf.h"

/*
 *	Macros definition
 */

#define SYSTEMTICK_PERIOD_MS  10

/*
 *	Type declaration
 */



/*
 *	Data declaration
 */

extern __IO uint32_t LocalTime; /* this variable is used to create a time reference incremented by 10ms */


/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void Time_Update(void);

void Delay(uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* _TIME_UPDATE_H */
