#ifndef __DISPLAY_H
#define __DISPLAY_H

#include <stdint.h>
#include "stm32f10x_conf.h"
#include "fonts/font.h"
#include "data.h"

#define SEC_1	5
#define SEC_2	10

typedef enum {
	NONE = 0x00,
	Brake_short = 0x01,
	Brake_long = 0x02,
	Thr_short = 0x03,
	Thr_long = 0x04,
	Both_long = 0x05
} BUTTON_STATE;

typedef struct {
   uint8_t	screen;
   uint8_t	subscreen;
   void             (*FillInfo[])();
} screens_t;

typedef enum {
	None		= 0x00,
	BrakeShort 	= 0x01,
	BrakeLong 	= 0x02,
	ThrShort	= 0x03,
	ThrLong		= 0x04,
	BothShort	= 0x05,
	BothLong	= 0x06
} M365_State;

typedef struct {
   uint16_t	brake;
   uint16_t	throttle;
} M365_control_t;


void drawTestScreen(void);
void UpdateScreen(void);


#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif

