/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdint.h>

#include "stm32f10x_conf.h"

#define COM1		0
#define COM2		1

#ifdef USEUSART1
#define DEFCOM COM1
#elif defined USEUSART2
#define DEFCOM COM2
#endif

#define DMA_BUFFER_SIZE	256

typedef struct	{
	uint16_t	Status;
	uint16_t	RxCount;
	uint16_t	TxCount;
	uint16_t	RxBufferSize;
} Serial_Status_t;

extern Serial_Status_t	SStatus[2];

#ifdef __cplusplus
extern "C" {
#endif

void SendToUSART(uint8_t, const uint8_t *, uint32_t);

void PrepareReceiveCOM(uint8_t usart, uint8_t *, int);

#ifdef __cplusplus
}
#endif

#endif
