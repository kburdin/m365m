/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DATA_H
#define __DATA_H

#include <stdint.h>

#include "stm32f10x_conf.h"

/*
 *	Macros definition
 */
#define ARRAY_LEN(x)            (sizeof(x) / sizeof((x)[0]))

#define _BIT_SET(x, number) 		( x |= ( 1 << number ) )
#define _BIT_RESET(x, number) 		( x &= ~( 1 << number ) )
#define _BIT_IS(x, number)		( ( x >> number ) & 1 )
#define _BIT_INSERT(x, number, value) 	{ x = ( ( value == 1 ) ? x | ( 1 << number ) : x & ( ~( 1 << number ) ) ); }

#define SET_BIT_M(x, a) 		x |= 1 << a
#define RESET_BIT_M(x, a) 		x &= ~(1 << a)
#define GET_BIT_M(x, a)			( (x >> a) & 1 )
#define WRITE_BIT_M(x, a, v)		x ^= (-v ^ x) & (1 << a)

#define Header0 0x55
#define Header1 0xAA
#define Ninebotread 0x01
#define Ninebotwrite 0x03
#define NinebotMaxPayload 0x38

#define Header0Pos	0
#define Header1Pos	1
#define LenPos	2
#define AddrPos	3
#define DirPos	4
#define CmdPos	5

#define INITLEN 2	//Два байта начала пакета 0х55 и 0хАА
#define CRCLEN 2	//Два байта контрольной суммы
#define DATA_BUFFER_SIZE 64

/*
 *	Type declaration
 */
typedef struct {
	uint16_t startCapacity;
	uint8_t startPercent;
	int16_t	maxCurrent;
	int16_t maxSpeed;
} trip_data_t;

typedef struct {
	const uint8_t data[9];
	uint8_t isEnabled;
} requests_t;

typedef struct {
	const uint8_t data[10];
	uint8_t isEnabled;
} commands_t;

#pragma pack(1)
typedef struct {
	uint16_t	ridescr;
	uint16_t	calcld;
	float		wheelmult;
	uint16_t	light;
	uint16_t 	crc;
} settings_t;

typedef struct { //header of receiving answer
	uint16_t neg;
	uint8_t len;
	uint8_t addr;
	uint8_t func;
	uint8_t cmd;
} ANSWER_HEADER;

typedef struct {
	uint8_t state;      //0-stall, 1-drive, 2-eco stall, 3-eco drive
	uint8_t battery;    //battery status 0 - min, 7(or 8...) - max
	uint8_t headLamp;   //0-off, 0x64-on
	uint8_t beepAction;
	//Эти два регистра добавляются в ПРО
	uint8_t speed;		//Скорость, отображаемая на панели
	uint8_t hz; 		//похоже, что это warnings на панели.
} A21C00HZ64;

typedef struct {
	uint8_t hz1;
	uint8_t throttle; //throttle
	uint8_t brake;    //brake
	uint8_t hz2;
	uint8_t hz3;
} A20C00HZ65;

typedef struct {
	char	serial[14];
	uint16_t bmsver;
	uint16_t capacity;
} A25C10;


typedef struct {
	uint16_t fullcharges;
	uint16_t charges;
} A25C1B;

typedef struct {
	uint8_t	day: 5;
	uint8_t month: 4;
	uint8_t year: 7;
} A25C20;


typedef struct {
	uint16_t remainCapacity;     //remaining capacity mAh
	uint8_t remainPercent;      //charge in percent
	uint8_t u4;                 //battery status??? (unknown)
	int16_t current;            //current        /100 = A
	int16_t voltage;            //batt voltage   /100 = V
	uint8_t temp1;              //-=20
	uint8_t temp2;              //-=20
} A25C31;

typedef struct {
	int16_t status; //Vida de la batería, 0-100, error si es menor a 60
} A25C3B;

typedef struct {
	int16_t cell[15]; //cell /1000
} A25C40;

//ESC data
typedef struct {
	char	serial[14];
	char	pin[6];
	uint16_t version;
} A23C10;

typedef struct {
	//32 bytes;
	uint16_t error;				// Normal = 0
	uint16_t warning;			// Normal = 0
	uint16_t state;				//0x0000 = normal, 0x0002 = blocked
	uint16_t u1;				//hz
	uint16_t batt;				// percent 0 - 100
	int16_t speed;              // /1000
	uint16_t averageSpeed;       // /1000
	uint32_t mileageTotal;       // /1000
	uint16_t mileageCurrent;     // /100
	uint16_t elapsedPowerOnTime; //time from power on, in seconds
	int16_t mainframeTemp;      // /10
	uint8_t u2[8];
} A23CB0;

typedef struct { //skip
	uint16_t remainMileage;  // /100
} A23C25;

typedef struct {
	uint16_t powerOnTime;
	uint16_t ridingTime;
} A23C3A;

typedef struct {
	int16_t i1;                           //mainframe temp
} A23C3E;

typedef struct {
	uint16_t kers;
	uint16_t cruise;
	uint16_t tailled;
} A23C7B;


typedef union {
	uint8_t buf[DATA_BUFFER_SIZE];
	A21C00HZ64 S21C00HZ64;
	A20C00HZ65 S20C00HZ65;
	A25C10 S25C10;
	A25C1B S25C1B;
	A25C20 S25C20;
	A25C31 S25C31;
	A25C3B S25C3B;
	A25C40 S25C40;
	A23C10 S23C10;
	A23C3E S23C3E;
	A23CB0 S23CB0;
	A23C25 S23C25;
	A23C3A S23C3A;
	A23C7B S23C7B;
} data_t;

typedef struct {
	ANSWER_HEADER head;
	data_t	data;
} packet_t;

typedef struct {
	A21C00HZ64 S21C00HZ64;
	A20C00HZ65 S20C00HZ65;
	A25C10 S25C10;
	A25C1B S25C1B;
	A25C20 S25C20;
	A25C31 S25C31;
	A25C3B S25C3B;
	A25C40 S25C40;
	A23C10 S23C10;
	A23C3E S23C3E;
	A23CB0 S23CB0;
	A23C25 S23C25;
	A23C3A S23C3A;
	A23C7B S23C7B;
} local_data_t;

#pragma pack()
/*
 *	Function declaration
 */
void checkRXData ( void );
uint16_t computeCRC16 ( uint8_t *buf, uint16_t len );

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif

